var LibHtml = {
    parse: (encoded) => new DOMParser().parseFromString(encoded, 'text/html').body.textContent,
    hide: (element)=> {
        return new Promise((resolve) => {
            element.classList.add('hide');
            resolve();
        });
    },
    show: (element)=> {
        return new Promise((resolve) => {
            element.classList.remove('hide');
            resolve();
        });
    },

    spinner: {
        spin: (options, promise)=> {
            LibHtml.spinner.errorStop(options.spinner)
                .then(() => LibHtml.spinner.disable(options.button))
                .then(() => LibHtml.spinner.start(options.spinner))
                .then(() => promise)
                .then(() => LibHtml.spinner.stop(options.spinner), () => LibHtml.spinner.error(options.spinner))
                .then(() => LibHtml.spinner.enable(options.button))
        },
        disable: (element)=> {
            return new Promise((resolve, reject) => {
                if (element) {
                    element.disabled = "disabled";
                }
                resolve();
            });
        },
        enable: (element)=> {
            return new Promise((resolve, reject) => {
                if (element) {
                    element.disabled = null;
                }
                resolve();
            });
        },
        start: (element)=> {
            return new Promise((resolve, reject) => {
                element.classList.add('spinner-on');
                resolve();
            });
        },
        stop: (element)=> {
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    element.classList.remove('spinner-on');
                    resolve();
                }, 1000);
            });
        },
        error: (element)=> {
            return new Promise((resolve, reject) => {
                element.classList.remove('spinner-on');
                element.classList.add('spinner-error');
                setTimeout(()=> {
                    LibHtml.spinner.errorStop(element);
                }, 3000);
                resolve();
            });
        },
        errorStop: (element)=> {
            return new Promise((resolve) => {
                element.classList.remove('spinner-error');
                resolve();
            });
        }

    },

    updateBookmarkSelect: (selectId)=> {
        return new Promise((resolve, reject) => {
            LibBookmarks.getTree().then((bookmarkItems) => {
                var bookmarkFolder = bookmarkItems[0];
                LibHtml.updateBookmarkSelectOption(selectId, bookmarkFolder);
                resolve();
            });
        });
    },

    updateBookmarkSelectOption: (selectId, bookmarkFolder, depth) => {
        depth = depth || 0;
        var htmlNode = document.getElementById(selectId);

        for (var bookmarkItem of bookmarkFolder.children) {
            if (!bookmarkItem.url) {
                var prefix = "";
                for (var i = 0; i < depth; ++i) {
                    prefix += "&nbsp;&nbsp;&nbsp;";
                }

                var option = document.createElement("option");
                option.setAttribute("value", bookmarkItem.id);
                option.innerHTML = prefix + bookmarkItem.title;

                htmlNode.appendChild(option);

                if (0 < bookmarkItem.children.length) {
                    LibHtml.updateBookmarkSelectOption(selectId, bookmarkItem, depth + 1);
                }

            }
        }
    }
};
