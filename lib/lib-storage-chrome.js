if ('undefined' !== typeof chrome) {
    LibStorage.get = (key, def) => {
        return new Promise((resolve)=> {
            chrome.storage.local.get(null, function (storage) {
                if (key) {
                    'undefined' === typeof storage[key] ? resolve(def) : resolve(storage[key]);
                } else {
                    resolve(storage);
                }
            })
        });
    };
    LibStorage.set = (obj) => {
        return new Promise((resolve)=> {
            chrome.storage.local.set(obj, function () {
                resolve();
            })
        });
    };
}