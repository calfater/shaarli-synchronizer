if ('undefined' !== typeof browser) {
    LibStorage.remove = (key) => browser.storage.local.remove(key);
    LibStorage.get = (key, def)=> {
        if ('undefined' !== typeof key && null !== key) {
            return new Promise((resolve)=> {
                LibStorage.get().then((storage)=> {
                    if ('undefined' === typeof storage[key]) {
                        // LibConsole.log("LibStorage: '" + key + "' => (def)", def);
                        resolve(def);
                    } else {
                        // LibConsole.log("LibStorage: '" + key + "' =>", storage[key]);
                        resolve(storage[key]);
                    }
                });
            });
        } else {
            return browser.storage.local.get(null);
        }
    };
    LibStorage.set = browser.storage.local.set;
}
