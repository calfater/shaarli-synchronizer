if ('undefined' !== typeof browser) {
    LibBookmarks.defaultId = 'toolbar_____';
    LibBookmarks.getTree = browser.bookmarks.getTree;
    LibBookmarks.getSubTree = browser.bookmarks.getSubTree;
    LibBookmarks.create = browser.bookmarks.create;
    LibBookmarks.get = browser.bookmarks.get;
    LibBookmarks.remove = browser.bookmarks.remove;

    LibBookmarks.addListener = (name, callback) => {
        if ("created" === name) {
            browser.bookmarks.onCreated.addListener(callback)
        } else if ("changed" === name) {
            browser.bookmarks.onChanged.addListener(callback)
        } else if ("removed" === name) {
            browser.bookmarks.onRemoved.addListener(callback)
        } else if ("moved" === name) {
            browser.bookmarks.onMoved.addListener(callback)
        }
    };

}
