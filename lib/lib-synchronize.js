var LibSynchronize = {
    running: false,

    syncLocalCache: () => {
        return LibSynchronize.getLocalLinkList()
            .then((data)=> LibStorage.set({localCache: data}))
            .then(LibSynchronize.dumpLocalCache);
    },

    syncRemoteCache: () => {
        return LibSynchronize.getRemoteLinkList().then((data)=> {
            return LibStorage.set({wsCache: data}).then(()=> {
                    return LibSynchronize.dumpRemoteCache();
                }
            )
        });
    },

    clearLocalCache: ()=> {
        return LibStorage.remove("localCache").then(() => {
                return new Promise((resolve) => {
                    LibConsole.log("localCache cleared");
                    resolve();
                });
            }
        )
    },

    clearRemoteCache: ()=> {
        return LibStorage.remove("wsCache").then(() => {
                return new Promise((resolve) => {
                    LibConsole.log("wsCache cleared");
                    resolve();
                });
            }
        )
    },

    dumpLocalCache: ()=> {
        return LibStorage.get('localCache').then((data)=> {
                return new Promise((resolve)=> {
                    LibConsole.log("localCache is ", data.length, data);
                    resolve();
                });
            }
        )
    },

    dumpRemoteCache: ()=> {
        return LibStorage.get('wsCache').then((data)=> {
                return new Promise((resolve)=> {
                    LibConsole.log("wsCache is ", data.length, data);
                    resolve();
                });
            }
        )
    },

    getItemsOrder: async (item, tree)=> {
        tree = [];
        if (!item) {
            var rootFolderId = await LibStorage.get('rootFolderId');
            await LibBookmarks.getSubTree(rootFolderId).then(async rootFolder => {
                item = rootFolder[0];
            })
        }
        for (let children of item.children) {
            if (!children.url) {
                tree.push(await LibSynchronize.getItemsOrder(children));
            } else {
                tree.push(children.title);
            }
        }
        return tree;
    },

    sync: async (options)=> {
        LibLogger.debug("Starting Sync");
        if (LibSynchronize.running) {
            return new Promise((resolve, reject) => {
                reject("Already syncing");
            });
        } else {
            LibSynchronize.running = true;
        }

        options = options ||
            {
                push: true,
                pull: true
            };

        return new Promise(async (resolve, reject)=> {
            //console.log(await LibSynchronize.getItemsOrder());
            LibConsole.log("Begin sync.");
            //var begin = new Date();
            //var config = await LibSynchronize.getRemoteConfig();
            var wsCache = await LibStorage.get("wsCache", []);
            var wsFresh = null;
            var localCache = await LibStorage.get("localCache", []);
            var localFresh = await LibSynchronize.getLocalLinkList();
            var createdOnServer = [];
            var removedFromServer = [];
            var createdOnLocal = [];
            var removedFromLocal = [];

            // if (null === wsCache) {
            //     await LibSynchronize.syncRemoteCache();
            //     wsCache = await LibStorage.get("wsCache", []);
            // }
            // if (null === localCache) {
            //     await LibSynchronize.syncLocalCache();
            //     localCache = await LibStorage.get("localCache", []);
            // }

            await LibSynchronize.getRemoteLinkList().then((data)=> {
                wsFresh = data;
            }, (err)=> {
                LibConsole.log("Sync failed : " + err);
                LibLogger.log("Sync failed : " + err);
                reject(err);
            });
            if (null === wsFresh) {
                LibSynchronize.running = false;
                reject("no ws response");
                return;
            }
            //if (null === config) {
            //    config = {serverId: null, order: await LibSynchronize.getItemsOrder()};
            //    await LibSynchronize.createShaarliLink({title: "shaarli-synchronizer-config", tag: "", description: JSON.stringify(config, null, 0)});
            //}

            if (options.pull) {
                for (let link of wsFresh) {
                    if (!LibSynchronize.findUniqueByCriteria(wsCache, {tag: link.tag, title: link.title, url: link.url})) {
                        createdOnServer.push(link);
                    }
                }

                for (let link of wsCache) {
                    if (!LibSynchronize.findUniqueByCriteria(wsFresh, {tag: link.tag, title: link.title, url: link.url})) {
                        removedFromServer.push(link);
                    }
                }
            }

            if (options.push) {
                for (let link of localFresh) {
                    // TODO: clean localFresh from same links with one of the without tag (empty tag)
                    if (!LibSynchronize.findUniqueByCriteria(localCache, {tag: link.tag, title: link.title, url: link.url})) {
                        if (!LibSynchronize.findUniqueByCriteria(createdOnLocal, {tag: link.tag, title: link.title, url: link.url})) {
                            createdOnLocal.push(link);
                        }
                    }
                }
                for (let link of localCache) {
                    if (!LibSynchronize.findUniqueByCriteria(localFresh, {tag: link.tag, title: link.title, url: link.url})) {
                        removedFromLocal.push(link);
                    }
                }
            }
            if (await LibStorage.get("debugMode") ) {
                LibConsole.log("--------------------------------------------------------------------------------");
                LibConsole.log("localCache", localCache.length, localCache.slice());
                LibConsole.log("localFresh", localFresh.length, localFresh.slice());
                LibConsole.log("wsCache", wsCache.length, wsCache.slice());
                LibConsole.log("wsFresh", wsFresh.length, wsFresh.slice());
                LibConsole.log("createdOnServer", createdOnServer.length, createdOnServer.slice());
                LibConsole.log("removedFromServer", removedFromServer.length, removedFromServer.slice());
                LibConsole.log("createdOnLocal", createdOnLocal.length, createdOnLocal.slice());
                LibConsole.log("removedFromLocal", removedFromLocal.length, removedFromLocal.slice());
            }
            for (let link of createdOnServer) {
                if (LibSynchronize.findUniqueByCriteria(localFresh, {tag: link.tag, title: link.title, url: link.url})) {
                    LibConsole.log("Already exists on local : ", {tag: link.tag, title: link.title, url: link.url});
                    wsCache.push(link);
                } else {
                    await LibSynchronize.createLocalLink(link).then((bmk) => {
                        wsCache.push(link);
                        localCache.push({localId: bmk.id, title: bmk.title, url: bmk.url, tag: link.tag});
                        localFresh.push({localId: bmk.id, title: bmk.title, url: bmk.url, tag: link.tag});
                    });
                }
            }
            for (let link of removedFromServer) {
                let criterion = {tag: link.tag, title: link.title, url: link.url};

                var localLinkToRemove = LibSynchronize.findUniqueByCriteria(localFresh, criterion);
                if (null !== localLinkToRemove) {
                    await LibSynchronize.removeLocalLink(localLinkToRemove.localId).then(()=> {
                        wsCache.splice(wsCache.indexOf(LibSynchronize.findUniqueByCriteria(wsCache, criterion)), 1);
                        localCache.splice(localCache.indexOf(LibSynchronize.findUniqueByCriteria(localCache, criterion)), 1);
                        localFresh.splice(localFresh.indexOf(LibSynchronize.findUniqueByCriteria(localFresh, criterion)), 1);
                    });
                } else {
                    // Already removed, probably by hand
                    wsCache.splice(wsCache.indexOf(LibSynchronize.findUniqueByCriteria(wsCache, criterion)), 1);
                }
            }
            if (0 < removedFromServer.length) {
                LibSynchronize.deleteEmptyFoldersLocal();
            }
            if (0 < removedFromServer.length || 0 < createdOnServer.length) {
                // update local order
            }
            for (let link of removedFromLocal) {
                let criterion = {tag: link.tag, title: link.title, url: link.url};

                let serverLinks = LibSynchronize.findListByCriteria(wsFresh, {title: link.title, url: link.url});
                let shaarliLinkToRemove = LibSynchronize.findUniqueByCriteria(wsFresh, criterion);

                if (shaarliLinkToRemove) {
                    if (1 === serverLinks.length) {
                        await LibSynchronize.removeShaarliLink(shaarliLinkToRemove).then(()=> {
                            wsCache.splice(wsCache.indexOf(LibSynchronize.findUniqueByCriteria(wsCache, criterion)), 1);
                            localCache.splice(localCache.indexOf(LibSynchronize.findUniqueByCriteria(localCache, criterion)), 1);
                            wsFresh.splice(wsFresh.indexOf(LibSynchronize.findUniqueByCriteria(wsFresh, criterion)), 1);
                        });
                    } else {
                        await LibSynchronize.removeShaarliTag(serverLinks[0].serverId, link.tag).then(()=> {
                            wsCache.splice(wsCache.indexOf(LibSynchronize.findUniqueByCriteria(wsCache, criterion)), 1);
                            localCache.splice(localCache.indexOf(LibSynchronize.findUniqueByCriteria(localCache, criterion)), 1);
                            wsFresh.splice(wsFresh.indexOf(LibSynchronize.findUniqueByCriteria(wsFresh, criterion)), 1);
                        });
                    }
                } else {
                    LibConsole.log("Remove Shaarli link : ", link);
                    await LibLogger.log("Remove Shaarli link : " + (link.tag ? (link.tag + "/") : "") + link.title);
                    console.warn("Already removed from server, probably by hand");
                    localCache.splice(localCache.indexOf(LibSynchronize.findUniqueByCriteria(localCache, criterion)), 1);
                }

            }

            for (let link of createdOnLocal) {
                let serverLinks = LibSynchronize.findListByCriteria(wsFresh, {title: link.title, url: link.url});

                if (0 === serverLinks.length) {
                    await LibSynchronize.createShaarliLink(link).then((newShaarliLink)=> {
                        localCache.push(link);
                        wsCache.push(newShaarliLink);
                        wsFresh.push(newShaarliLink);
                    });
                } else {
                    await LibSynchronize.addShaarliTag(serverLinks[0].serverId, link.tag).then((newShaarliLink)=> {
                        localCache.push(link);
                        wsCache.push(newShaarliLink);
                        wsFresh.push(newShaarliLink);
                    });
                }
            }
            //if (0 < removedFromLocal.length || 0 < createdOnLocal.length) {
            //    LibSynchronize.getItemsOrder().then((itemsOrder) => {
            //        config.order = itemsOrder;
            //        return LibSynchronize.setRemoteConfig(config);
            //    });
            //}


            if (await LibStorage.get("debugMode")) {
                LibConsole.log("--------------------------------------------------------------------------------");
                LibConsole.log("localCache", localCache.length, localCache.slice());
                // LibConsole.log("localFresh", localFresh.length, localFresh.slice());
                LibConsole.log("wsCache", wsCache.length, wsCache.slice());
                // LibConsole.log("wsFresh", wsFresh.length, wsFresh.slice());
                // LibConsole.log("createdOnServer", createdOnServer.length, createdOnServer.slice());
                // LibConsole.log("removedFromServer", removedFromServer.length, removedFromServer.slice());
                // LibConsole.log("createdOnLocal", createdOnLocal.length, createdOnLocal.slice());
                // LibConsole.log("removedFromLocal", removedFromLocal.length, removedFromLocal.slice());
            }

            //await LibStorage.set({wsCache: wsFresh});
            //await LibStorage.set({localCache: localFresh});
            await LibStorage.set({wsCache: wsCache});
            await LibStorage.set({localCache: localCache});

            //await LibSynchronize.removeShaarliLink({serverId: config.serverId, title: "shaarli-synchronizer-config"});
            //config.order = await LibSynchronize.getItemsOrder();
            //await LibSynchronize.createShaarliLink({title: "shaarli-synchronizer-config", tag: "", description: JSON.stringify(config, null, 0)});


            LibConsole.log("End sync.");
            LibSynchronize.running = false;
            resolve();
        });
    },

    watch: async () => {
        await LibBookmarks.addListener('created', async (id, bookmark) => {
            LibSynchronize.sync({push: true})
        });
        await LibBookmarks.addListener('changed', async (id, info) => {
            LibSynchronize.sync({push: true})
        });
        await LibBookmarks.addListener('removed', async (id, info) => {
            LibSynchronize.sync({push: true})
        });
        await LibBookmarks.addListener('moved', async (id, info) => {
            LibSynchronize.sync({push: true})
        });
    },

    getRemoteLinkList: () => {
        return new Promise((resolve, reject)=> {
            LibWs.call("links?limit=all", {json: true}).then((data)=> {
                var links = [];
                for (let link of data) {
                    if (LibSynchronize.isABookmark(link)) {
                        if (0 === link.tags.length) {
                            links.push({serverId: link.id, title: link.title, url: link.url, tag: ""});
                        } else {
                            for (let tag of link.tags) {
                                links.push({serverId: link.id, title: link.title, url: link.url, tag: tag});
                            }
                        }
                    }
                }
                resolve(links);
            }, (err)=> {
                reject(err);
            });
        })
    },

    getRemoteConfig: () => {
        return LibWs.call("links?searchterm=shaarli-synchronizer-config", {json: true})
            .then((data)=> new Promise((resolve, reject)=> {
                for (let link of data) {
                    if (link.title === "shaarli-synchronizer-config") {
                        resolve(JSON.parse(LibHtml.parse(link.description)));
                        return;
                    }
                }
                resolve({});
            }));
    },

    setRemoteConfig: (config) => {
        return LibWs.call("links?searchterm=shaarli-synchronizer-config", {json: true})
            .then((data)=> new Promise((resolve, reject)=> {
                for (let link of data) {
                    if (link.title === "shaarli-synchronizer-config") {
                        resolve(link.id);
                        return;
                    }
                }
                resolve(null);
            }))
            .then((serverId) => {
                if (null === serverId) {
                    return LibWs.call("links", {method: "POST", json: true, data: {title: 'shaarli-synchronizer-config', description: JSON.stringify(config, null, 1)}});
                } else {
                    return LibWs.call("links/" + serverId, {method: "PUT", json: true, data: {title: 'shaarli-synchronizer-config', description: JSON.stringify(config, null, 1)}});
                }
            });

    },

    getLocalLinkList: () => {
        return new Promise(async (resolve)=> {
            var rootFolder;
            await LibStorage.get('rootFolderId').then(LibBookmarks.getSubTree).then((folder) => {
                rootFolder = folder[0];
            });
            resolve(LibSynchronize.getLocalLinkList_(rootFolder, ""));
        })
    },

    getLocalLinkList_: (rootFolder, tag) => {
        var linkList = [];
        for (var node of rootFolder.children) {
            if (!node.url) {
                linkList.push(...LibSynchronize.getLocalLinkList_(node, (tag ? tag + "/" : "") + node.title.replace(/ /g, "-")));
            } else {
                linkList.push({localId: node.id, url: node.url, title: node.title, tag: tag});
            }
        }
        return linkList;
    },

    findUniqueByCriteria: (objectList, criterion)=> {
        var find = LibSynchronize.findListByCriteria(objectList, criterion);
        return 0 === find.length ? null : find[0];
    },

    findListByCriteria: (objectList, criterion)=> {
        let ret = [];
        for (let obj of objectList) {
            let match = true;
            for (let criteria in criterion) {
                var criteriaValue = criterion[criteria];
                var objValue = obj[criteria];
                if (criteria === 'url') {
                    criteriaValue = criteriaValue.replace(/[ ?/]*/g, "")
                    objValue = objValue.replace(/[ ?/]*/g, "")
                }
                if (!obj.hasOwnProperty(criteria) || objValue !== criteriaValue) {
                    match = false;
                }
            }
            if (match) {
                ret.push(obj);
            }
        }
        return ret;
    },

    createLocalLink: async (link)=> {
        LibConsole.log("Create local : ", link);
        await LibLogger.log("Create local : " + (link.tag ? (link.tag + "/") : "") + link.title);
        var localFolder = await LibSynchronize.getLocalFolder(link.tag);
        return LibBookmarks.create({parentId: localFolder.id, title: link.title, url: link.url});
    },

    removeLocalLink: async (linkId) => {
        await LibBookmarks.get(linkId).then(async (bmk)=> {
            LibConsole.log("Remove Local bookmark : ", bmk[0]);
            await LibLogger.log("Remove Local bookmark : " + bmk[0].title);
        });
        return LibBookmarks.remove(linkId);
    },

    removeShaarliLink: async (link) => {
        LibConsole.log("Remove Shaarli link : ", link);
        await LibLogger.log("Remove Shaarli link : " + (link.tag ? (link.tag + "/") : "") + link.title);
        return LibWs.call("links/" + link.serverId, {method: "DELETE"});
    },

    createShaarliLink: (link) => {
        return new Promise(async (resolve, reject) => {
            var tags = ( "" !== link.tag) ? [link.tag.replace("/ /g", "-")] : [];
            var data = {
                "url": link.url,
                "title": link.title,
                "description": link.description,
                "tags": tags,
                "private": true
            };

            LibConsole.log("Create Shaarli link : ", data);
            await LibLogger.log("Create Shaarli link : " + (link.tag ? (link.tag + "/") : "") + link.title);
            LibWs.call("links", {method: "POST", json: true, data: data}).then((resp)=> {
                resolve({serverId: resp.id, tag: link.tag, title: resp.title, url: resp.url});
            }, reject);

        });

    },

    addShaarliTag: (serverId, newTag) => {
        return new Promise((resolve) => {

            LibConsole.log("Add Shaarli link tags : ", serverId, newTag);
            LibWs.call("links/" + serverId, {method: "GET", json: true}).then((existingLink)=> {
                var tags = [];
                for (let tag of existingLink.tags) {
                    tags.push(tag)
                }
                if (-1 === tags.indexOf(newTag)) {
                    LibLogger.log("Add Shaarli link tags : ", newTag + " to " + existingLink.title);
                    tags.push(newTag);
                } else {
                    LibConsole.log("Tag already exists : ", serverId, newTag);
                    LibLogger.log("Shaarli link tags already exists: ", newTag + " to " + existingLink.title);
                }
                LibWs.call("links/" + serverId, {
                    method: "PUT",
                    json: true,
                    data: {
                        "url": existingLink.url,
                        "title": existingLink.title,
                        "description": existingLink.description,
                        "tags": tags,
                        "private": true
                    }
                }).then((resp)=> {
                    resolve({serverId: resp.id, tag: newTag, title: resp.title, url: resp.url});
                });
            });

        });

    },

    removeShaarliTag: (serverId, oldTag) => {
        return new Promise((resolve, reject) => {

            LibConsole.log("Remove Shaarli link tags : ", serverId, oldTag);
            LibWs.call("links/" + serverId, {method: "GET", json: true}).then((existingLink)=> {
                LibLogger.log("Remove Shaarli link tags : ", oldTag + " from " + existingLink.title);
                var tags = [];
                for (let tag of existingLink.tags) {
                    if (tag !== oldTag) {
                        tags.push(tag);
                    }
                }
                LibWs.call("links/" + serverId, {
                    method: "PUT",
                    json: true,
                    data: {
                        "url": existingLink.url,
                        "title": existingLink.title,
                        "description": existingLink.description,
                        "tags": tags,
                        "private": true
                    }
                }).then((resp)=> {
                    resolve({serverId: resp.id, tag: oldTag, title: resp.title, url: resp.url});
                }, reject);
            }, reject);
        });

    },

    getLocalFolder: async(tag, parentFolder) => {
        var explodedTag = tag ? tag.replace(/-/g, " ").split("/") : [];
        if (!parentFolder) {
            await LibStorage.get('rootFolderId').then(LibBookmarks.getSubTree).then((folder) => {
                parentFolder = folder[0];
            });
        }
        if (!tag) {
            return parentFolder;
        }
        for (var folder of parentFolder.children) {
            if (folder.title === explodedTag[0]) {
                explodedTag.shift();
                return await LibSynchronize.getLocalFolder(explodedTag.join("/"), folder);
            }
        }
        var newFolder = null;
        await LibBookmarks.create({parentId: parentFolder.id, title: explodedTag[0]}).then(async (f) => {
            await LibBookmarks.getSubTree(f.id).then((f2)=> {
                newFolder = f2[0];
            })
        });
        explodedTag.shift();
        return await LibSynchronize.getLocalFolder(explodedTag.join("/"), newFolder);
    },

    isABookmark: (serverBookmark) => {
        return -1 === serverBookmark.url.indexOf(serverBookmark.shorturl);
    },

    deleteEmptyFoldersLocal: () => {
        return new Promise((mainResolve)=> {
            LibStorage.get('rootFolderId')
                .then(async (rootFolderId) => {
                    await LibBookmarks.getSubTree(rootFolderId).then(async rootFolder => {
                        await LibSynchronize.deleteEmptyFoldersLocal_(rootFolder[0]);
                        mainResolve();
                    })
                })
        });
    },

    deleteEmptyFoldersLocal_: async (folder) => {
        for (var bmkNode of folder.children) {
            if (!bmkNode.url) {
                await LibSynchronize.deleteEmptyFoldersLocal_(bmkNode);
            }
        }
        await LibBookmarks.getSubTree(folder.id).then((f)=> {
            folder = f[0]
        });
        if (0 === folder.children.length) {
            LibConsole.log("Remove local folder : ", folder.title);
            await LibBookmarks.remove(folder.id);
        }
    }

};
