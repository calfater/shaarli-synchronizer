var LibLogger = {
    INFO: 1,
    DEBUG: 2,
    level: 1,
    maxLogSize: 500,
    clear: () => LibStorage.set({logs: []}),
    setLevel: (level) => {
        LibLogger.level = level;
    },
    debug: async (...msgs) => {
        if (LibLogger.level >= LibLogger.DEBUG) {
            return LibLogger.log(...msgs);
        }
    },
    log: async(...msgs) => {
        return LibStorage.get("logs", [])
            .then((logs) => new Promise((resolve)=> {
                var newLine = "";
                for (let msg of msgs) {
                    newLine += (newLine ? " " : "");
                    if ("object" === typeof msg) {
                        newLine += JSON.stringify(msg).replace('\\"', '"');
                    } else {
                        newLine += msg;
                    }
                }
                const date = new Date();
                logs.push(
                    date.getFullYear()
                    + "-" + ("0" + (date.getMonth() + 1)).slice(-2)
                    + "-" + ("0" + date.getDate()).slice(-2)
                    + " " + ("0" + date.getHours()).slice(-2)
                    + ":" + ("0" + date.getMinutes()).slice(-2)
                    + ":" + ("0" + date.getSeconds()).slice(-2)
                    + " - " + newLine);
                logs.splice(0, Math.max(0, logs.length - LibLogger.maxLogSize));
                LibStorage.set({logs: logs}).then(resolve);
            }))
    }
};
