var LibJWT = {

    base64url: (source) => {
        // Encode in classical base64
        encodedSource = CryptoJS.enc.Base64.stringify(source);

        // Remove padding equal characters
        encodedSource = encodedSource.replace(/=+$/, '');

        // Replace characters according to base64url specifications
        encodedSource = encodedSource.replace(/\+/g, '-');
        encodedSource = encodedSource.replace(/\//g, '_');

        return encodedSource;
    },

    getToken: function (header, payload, apiKey) {

        //LibConsole.log(header, payload, apiKey);

        var stringifiedHeader = CryptoJS.enc.Utf8.parse(JSON.stringify(header));
        var encodedHeader = LibJWT.base64url(stringifiedHeader);
        //LibConsole.log(encodedHeader);

        var stringifiedPayload = CryptoJS.enc.Utf8.parse(JSON.stringify(payload));
        var encodedPayload = LibJWT.base64url(stringifiedPayload);
        //LibConsole.log(encodedPayload);

        var signature = CryptoJS.HmacSHA512(encodedHeader + "." + encodedPayload, apiKey);
        signature = LibJWT.base64url(signature);
        //LibConsole.log(signature);

        var token = encodedHeader + "." + encodedPayload + '.' + signature;
        //LibConsole.log(token);
        return token;
    }

};
