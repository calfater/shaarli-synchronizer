var LibWs = {
    buildToken: apiKey => {
        var header = {"alg": "HS512", "typ": "JWT"};
        var payload = {"iat": Math.round(new Date().getTime() / 1000) - 60};
        var token = LibJWT.getToken(header, payload, apiKey);
        return token;
    },

    call: (what, options) => {
        options = options || {};
        options.json = options.json || false;
        options.data = options.data || null;
        options.method = options.method || "GET";
        return new Promise((resolve, reject)=> {
            LibStorage.get().then((storage) => {
                if (!storage.serverUrl) {
                    reject("No server url configured");
                    return;
                }
                if (!storage.apiKey) {
                    reject("No api key configured");
                    return;
                }

                var xhr = new XMLHttpRequest();
                var url = storage.serverUrl + '/api/v1/' + what;
                xhr.open(options.method, url, true);
                //xhr.responseType = 'document';

                xhr.onreadystatechange = function () {
                    if (this.readyState === XMLHttpRequest.DONE) {
                        if (this.status === 200 || this.status === 201 || this.status === 204 || this.status === 409) {
                            if (options.json) {
                                resolve(JSON.parse(this.responseText));
                            } else {
                                resolve(this.responseText);
                            }
                        } else {
                            reject("http code is '" + this.status + "', body is '" + this.responseText + "'");
                        }
                    }
                };
                xhr.setRequestHeader('Authorization', 'Bearer ' + LibWs.buildToken(storage.apiKey));
                xhr.setRequestHeader('Content-Type', 'application/json');
                xhr.send(options.data ? JSON.stringify(options.data) : options.data);
            })
        });
    }

}
