if ('undefined' !== typeof chrome) {
    LibBookmarks.defaultId = '1';
    LibBookmarks.getTree = ()=> {
        return new Promise((resolve) => {
            chrome.bookmarks.getTree((tree)=> {
                resolve(tree);
            })
        });
    };
    LibBookmarks.getSubTree = (id)=> {
        return new Promise((resolve) => {
            chrome.bookmarks.getSubTree(id, (tree)=> {
                resolve(tree);
            })
        });
    };
    LibBookmarks.create = (bm)=> {
        return new Promise((resolve) => {
            chrome.bookmarks.create(bm, (tree)=> {
                resolve(tree);
            })
        });
    };
    LibBookmarks.get = (id)=> {
        return new Promise((resolve) => {
            chrome.bookmarks.get(id, (tree)=> {
                resolve(tree);
            })
        });
    };
    LibBookmarks.remove = (id)=> {
        return new Promise((resolve) => {
            chrome.bookmarks.remove(id, ()=> {
                resolve();
            })
        });
    };
    
    LibBookmarks.addListener = (name, callback) => {
        if ("created" === name) {
            chrome.bookmarks.onCreated.addListener(callback)
        } else if ("changed" === name) {
            chrome.bookmarks.onChanged.addListener(callback)
        } else if ("removed" === name) {
            chrome.bookmarks.onRemoved.addListener(callback)
        } else if ("moved" === name) {
            chrome.bookmarks.onMoved.addListener(callback)
        }
    };

}
