(() => {
    document.addEventListener('DOMContentLoaded', initPopup);


    async function initPopup() {

        LibStorage.get().then(fillHtmlPlaceholders);

        document.getElementById("synchronize").addEventListener("click", LibSynchronize.sync);

    }

    function fillHtmlPlaceholders(options) {
        return new Promise((resolve)=> {
            LibBookmarks.get(options.rootFolderId).then((folder)=> {
                for (var elt of document.getElementsByClassName('rootBookmarkFolderName')) {
                    elt.innerHTML = folder[0].title;
                }
                resolve(options);
            });
        });
    }

})();
