(() => {
    LibLogger.log("Watching...");
    LibSynchronize.watch();

    var syncAndSchedule = async () => {
        if (await LibStorage.get("autoSync", false)) {
            await resetTimer();
            LibSynchronize.sync().then(schedule, schedule);
        } else {
            schedule();
        }
    };

    let schedule = () => {
        setTimeout(backgroundWatcher, 1000);
    };

    let backgroundWatcher = async()=> {
        let autoSyncTimer = await LibStorage.get("autoSyncTimer", 0);
        let autoSync = await LibStorage.get("autoSync", false);
        let autoSyncWaitingTime = await LibStorage.get("autoSyncWaitingTime") + 1;
        await LibStorage.set({autoSyncWaitingTime: autoSyncWaitingTime});

        if (!autoSync) {
            // Next loop, if autoSync is enabled, Sync will be done
            await LibStorage.set({autoSyncWaitingTime: autoSyncTimer - 1});
            schedule();
            return;
        }

        if (0 < autoSyncTimer && await LibStorage.get("autoSyncWaitingTime") >= autoSyncTimer) {
            syncAndSchedule();
        } else {
            schedule();
        }
    };

    LibStorage.get().then((storage) => {
        if (!storage.serverUrl || !storage.apiKey) {
            LibRuntime.openOptionsPage();
        }
    });


    LibBrowserAction.onClicked.addListener((evt)=> {
        //LibSynchronize.sync();
        LibRuntime.openOptionsPage();

    });

    var resetTimer = async()=> {
        await LibStorage.set({autoSyncWaitingTime: 0});
    };

    syncAndSchedule();

})();
