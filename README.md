# Shaarli synchronizer

> Shaarli synchronizer is a Firefox and Chrome plugin which do a *2-ways* sync between a [Shaarli][shaarli] server and your Browser's' bookmarks.

## Shaarli
 
> [Shaarli][shaarli] is a minimalist link sharing service that you can install on your own server. It is designed to be personal (single-user), fast and handy.
 
### Configuration

The api must be enabled on the server.
 
## The plugin

> This plugin works both on Firefox and Chrome.

### Installation

Plugins downloads are available for [Firefox](https://addons.mozilla.org/fr/firefox/addon/shaarli-synchronizer/) and [Chrome]()

### Configuration

Fill the `Server url`, and the `Api key`. You may also change the default `Bookmark root` folder where the synchronisation is done.

The *2-ways sync* will start in a few seconds.

[shaarli]:https://github.com/shaarli/Shaarli