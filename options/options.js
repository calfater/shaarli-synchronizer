(async () => {
    //console.log(await LibSynchronize.getRemoteConfig());
    //console.log(await LibSynchronize.setRemoteConfig({yolo: "yala"}));

    document.addEventListener('DOMContentLoaded', init);

    function init() {

        LibHtml.updateBookmarkSelect('rootFolderId').then(()=>dataBindSelect('rootFolderId', 'rootFolderId', LibBookmarks.defaultId));
        dataBindSelect('debugModeId', 'debugMode', false, (v) => "true" === v);
        dataBindSelect('autoSyncId', 'autoSync', true, (v) => "true" === v);
        dataBindInputText('autoSyncTimerId', 'autoSyncTimer', 60, (i)=> i ? parseInt(i) : 0);
        dataBindInputText('serverUrlId', 'serverUrl', "");
        dataBindInputText('apiKeyId', 'apiKey', "");

        //var pResolve = function (msg) {
        //    return function () {
        //        return new Promise((resolve, reject) => {
        //            console.log("resolve ", msg);
        //            throw Error("Yala")
        //            resolve("pResolve " + msg);
        //
        //        });
        //    }
        //};
        //var pReject = function (msg) {
        //    return function () {
        //        return new Promise((resolve, reject) => {
        //            console.log("reject ", msg);
        //            reject("pReject " + msg);
        //        });
        //    }
        //};
        //
        //
        //var log = (msg)=> {
        //    return () => {
        //        console.log('log ' + msg);
        //    }
        //};
        //
        //pReject('1<-')().then(log('called'));
        //pResolve('1<-')()
        //    .then(pResolve('2<-'))
        //    .then(pResolve('3<-'))
        //    .then(pReject('4<-'))
        //    .then(pReject('5<-'))
        //    .then(pResolve('6<-'))
        //    .then(pReject('7<-'))
        //    .then(pResolve('<-8'))
        //    .catch((v)=>log(v)())
        //    .then(pReject('9<-'), pReject('->9'))
        //    .then(pReject('10<-'), pReject('->10'))
        //    .then(pReject('11<-'), pResolve('->11'))
        //    .then(pResolve('12<-'))
        //;

        document.getElementById('syncNow').addEventListener('click', (evt) => {
            LibHtml.spinner.spin({spinner: evt.target.nextElementSibling, button: evt.target}, new Promise((resolveSpinner, rejectSpinner) => {
                LibSynchronize.sync()
                    .then(() => updateLogs(true))
                    .then(resolveSpinner, rejectSpinner)
            }));
        });

        document.getElementById('debugModeId').addEventListener('change', () => {
            window.location.reload();
        });

        document.getElementById('printStorageData').addEventListener('click', (evt) => {
            LibHtml.spinner.spin({spinner: evt.target.nextElementSibling, button: evt.target}, new Promise((resolveSpinner, rejectSpinner) => {
                LibStorage.get()
                    .then(LibConsole.log)
                    .then(resolveSpinner, rejectSpinner)
            }));

        });

        document.getElementById('rebuildLocalCache').addEventListener('click', (evt) => {
            LibHtml.spinner.spin({spinner: evt.target.nextElementSibling, button: evt.target}, new Promise((resolveSpinner, rejectSpinner) => {
                LibSynchronize.syncLocalCache()
                    .then(() => updateLogs(true))
                    .then(resolveSpinner, rejectSpinner)
            }));
        });

        document.getElementById('rebuildRemoteCache').addEventListener('click', (evt) => {
            LibHtml.spinner.spin({spinner: evt.target.nextElementSibling, button: evt.target}, new Promise((resolveSpinner, rejectSpinner) => {
                LibSynchronize.syncRemoteCache()
                    .then(() => updateLogs(true))
                    .then(resolveSpinner, rejectSpinner);
            }));

        });

        document.getElementById('rebuildCache').addEventListener('click', (evt) => {
            LibHtml.spinner.spin({spinner: evt.target.nextElementSibling, button: evt.target}, new Promise((resolveSpinner, rejectSpinner) => {
                LibSynchronize.syncRemoteCache()
                    .then(LibSynchronize.syncLocalCache)
                    .then(() => updateLogs(true))
                    .then(resolveSpinner, rejectSpinner)
            }));
        });

        document.getElementById('removeCache').addEventListener('click', (evt) => {
            LibHtml.spinner.spin({spinner: evt.target.nextElementSibling, button: evt.target}, new Promise((resolveSpinner, rejectSpinner) => {
                LibSynchronize.clearLocalCache()
                    .then(LibSynchronize.clearRemoteCache)
                    .then(() => updateLogs(true))
                    .then(resolveSpinner, rejectSpinner);
            }))
        });

        LibStorage.get("debugMode").then((debug) => {
            if (debug) {
                while (0 < document.getElementsByClassName('showIfDebugEnabled').length) {
                    document.getElementsByClassName('showIfDebugEnabled')[0].classList.remove('showIfDebugEnabled');
                }
            }
        });

        document.getElementById('logsClear').addEventListener('click', () => {
            LibLogger.clear().then(()=>updateLogs(true));
        });

        updateLogs(true);

        setInterval(() => updateLogs(false), 1000);

    }

    function updateLogs(scroll) {
        return LibStorage.get("logs", [])
            .then((logs) => {
                return new Promise((resolve)=> {
                    var logElt = document.getElementById("logs");
                    logElt.innerHTML = "";
                    for (let log of logs) {
                        var li = document.createElement("li");
                        li.innerHTML = log;
                        logElt.appendChild(li)
                    }
                    if (scroll) {
                        logElt.parentNode.scrollTop = logElt.parentNode.scrollHeight;
                    }
                    resolve();
                })
            })
    }

    function dataBindInputText(elementId, storageKey, defaultValue, converter) {
        defaultValue = defaultValue || "";
        converter = converter || function (p) {
                return p
            };
        var elt = document.getElementById(elementId);
        return new Promise((resolve) => {
            LibStorage.get(storageKey).then((storageValue)=> {
                if ('undefined' === typeof storageValue) {
                    storageValue = defaultValue;
                    var a = {};
                    a[storageKey] = defaultValue;
                    LibStorage.set(a);
                }
                elt.value = storageValue;
                elt.addEventListener("keyup", (evt) => {
                    var a = {};
                    a[storageKey] = converter(evt.target.value);
                    LibStorage.set(a);
                });
                elt.addEventListener("change", (evt) => {
                    var a = {};
                    a[storageKey] = converter(evt.target.value);
                    LibStorage.set(a);
                });

            });
            resolve();
        });
    }

    function dataBindInputRadio(elementName, storageKey, defaultValue) {
        defaultValue = defaultValue || "";

        return new Promise((resolve) => {
            LibStorage.get(storageKey).then((value)=> {
                if (!value) {
                    value = value || defaultValue;
                    var a = {};
                    a[storageKey] = value;
                    LibStorage.set(a);
                }
                for (var radio of document.querySelectorAll("*[name='" + elementName + "'")) {
                    radio.addEventListener("change", (evt) => {
                        var a = {};
                        a[storageKey] = evt.target.value;
                        LibStorage.set(a);
                    });

                    if (radio.value === value) {
                        radio.setAttribute("checked", "checked");
                    }
                }
                resolve();
            });
        });
    }

    function dataBindSelect(elementId, storageKey, defaultValue, converter) {
        converter = converter || function (v) {
                return v;
            }
        return new Promise((resolve) => {
            LibStorage.get(storageKey).then((storageValue)=> {
                var select = document.getElementById(elementId);
                var selectValue = storageValue;

                if (undefined === selectValue) {
                    selectValue = defaultValue;
                    var a = {};
                    a[storageKey] = defaultValue;
                    LibStorage.set(a);
                }

                for (var i = 0; i < select.options.length; i++) {
                    var option = select.options[i];
                    if (option.value === "" + selectValue) {
                        option.setAttribute("selected", "selected");
                    }
                }

                select.addEventListener("change", (evt) => {
                    var selected = evt.target.options[evt.target.selectedIndex].value;
                    var a = {};
                    a[storageKey] = converter(selected);
                    LibStorage.set(a);
                });

            });
            resolve();
        });
    }


})();
